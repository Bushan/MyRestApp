package com.myrestapp.controllers;

import java.io.File;
import java.io.IOException;

import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myrestapp.valueobjects.Data;

@EnableCircuitBreaker
@RestController
//@RequestMapping(value="/rest/save")
public class SaveJson {
	
	@RequestMapping("/save")
	public Data saveData() {

		Data data = new Data();
		
		data.setAge(21);
		data.setName("Java");
		
		System.out.println("saving data....");
		generateJSON(data);
		return data;
	}
	
	public void generateJSON(Data data) {

		ObjectMapper mapper = new ObjectMapper();
		
		//Object to JSON in file
		try {
			mapper.writeValue(new File("file.json"), data);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
