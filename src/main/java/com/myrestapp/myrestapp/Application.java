package com.myrestapp.myrestapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.ComponentScan;

import com.myrestapp.controllers.SaveJson;

@EnableCircuitBreaker
@SpringBootApplication
@ComponentScan(basePackageClasses=SaveJson.class)
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
